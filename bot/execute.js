const BOARD_SIZES = [ [7, 6], [6, 5] ]
const BOARD_SIZE = BOARD_SIZES[1]

const SAMSUNG_S7 = {
  resolution_x: 1080,
  resolution_y: 1920,
  board: {
    height: 883,
    top_left_orb_x: 96,
    top_left_orb_y: 87,
    orb_center_delta : 177
  }
}

var adb = require('adbkit')
var client = adb.createClient()
var device = SAMSUNG_S7
var board = device.board

var path = [3,5,3,5,6,6,7,2,0,1,6,0,7,4,4,3,4,5,2,0]
var init_cursor = {"row":3,"col":4}

var top_left_orb_x = 96
var top_left_orb_y = 1120
var lateral_orb_delta = 177
var bilateral_orb_delta = Math.round(Math.sqrt(Math.pow(177, 2) * 2))

// case 0:              rc.col += 1; break; //      right
// case 1: rc.row += 1; rc.col += 1; break; // down right
// case 2: rc.row += 1;              break; // down
// case 3: rc.row += 1; rc.col -= 1; break; // down left
// case 4:              rc.col -= 1; break; //      left
// case 5: rc.row -= 1; rc.col -= 1; break; // up   left
// case 6: rc.row -= 1;              break; // up
// case 7: rc.row -= 1; rc.col += 1; break; // up   right

const PATH_XY_DICTIONARY = [
  [1, 0],
  [1, 1],
  [0, 1],
  [-1, 1],
  [-1, 0],
  [-1, -1],
  [0, -1],
  [1, -1],
].map((xy) => {return xy.map((d) => {return d * lateral_orb_delta})})

client.listDevices()

  .then(function(devices) {
  	var device_id = devices[0].id
  	return client.openMonkey(device_id)
  })

  .then(function(monkey) {

  	const SLEEP_MS = 50

  	var initial_orb_x = top_left_orb_x + (177 * init_cursor.col)
  	var initial_orb_y = top_left_orb_y + (177 * init_cursor.row)
	var cursor = [initial_orb_x, initial_orb_y]

  	var multi = monkey.multi()

  	var xys = [ ]
  	var xys_deltas = path.map((p) => {return PATH_XY_DICTIONARY[p]})
  	xys_deltas.forEach((xys_delta) => {
  		cursor[0] += xys_delta[0]
  		cursor[1] += xys_delta[1]
  		xys.push(cursor.slice())
  	})


  	multi
  	  .touchDown(initial_orb_x, initial_orb_y)
  	  .sleep(SLEEP_MS)

  	xys.forEach((xy) => {
  	  multi
  	    .touchMove(xy[0], xy[1])
  	    .sleep(SLEEP_MS)
  	})

  	multi
  	  .touchUp(cursor[0], cursor[1])

	multi.execute(function (err) {
	  if (err) throw err;
	  console.log('Complete')
	  // monkey.end()
	  // monkey.done((err) => {
	  monkey.quit((err) => {
	  	if (err) throw err;
	  })
	})
  })

  .catch(function(err) { console.error('Something went wrong:', err.stack) })