// NOTES:
// - x, 

var fs = require('fs')
var lwip = require('lwip')
var Promise = require('bluebird')

const BOARD_SIZES = [ [7, 6], [6, 5] ]
const BOARD_SIZE = BOARD_SIZES[1]
const SCREENCAP_PATH = 'screencap.png'
const OUTPUT_PATH = 'output.png'
const LEO_ANDROID_ID = '3e6a404e'
const ORB_OPTIMIZER_DICTIONARY = {
  '[250, 250, 130]': 3,
  '[250, 120, 80]': 0,
  '[80, 220, 110]': 2,
  '[70, 160, 250]': 1,
  '[230, 60, 150]': 5,
  '[170, 90, 180]': 4
}

const SAMSUNG_S7 = {
  resolution_x: 1080,
  resolution_y: 1920,
  board: {
    height: 883,
    top_left_orb_x: 96,
    top_left_orb_y: 87,
    orb_center_delta : 177
  }
}

var adb = require('adbkit')
var client = adb.createClient()
var device = SAMSUNG_S7
var board = device.board

function roundNearestPowerOfTen(value) { return Math.round( value / 10 ) * 10 }

client.listDevices()

  .then(function(devices) {
  	var device_id = devices[0].id
  	return client.screencap(device_id)
  })

  .then(function(png_stream) {

    png_stream.pipe(fs.createWriteStream(SCREENCAP_PATH))

    png_stream.on('end', function () {

      lwip.open(SCREENCAP_PATH, function (err, image) {
      // lwip.open('manual_screencap.png', function (err, image) {

        if (err) console.log(err)

        // Identify orbs by center pixel rgb 
        image.batch()
          // .crop(left, top, right, bottom, cb)
          // .crop(0, device.resolution_y - 883, device.resolution_x, device.resolution_y)
          .crop(0, device.resolution_y - board.height, device.resolution_x, device.resolution_y)
          .blur(3)
          .exec(function(err, image) {
            if (err) { console.log(err) }

            var [x_max, y_max] = BOARD_SIZE.slice()
            var orbs = []
            var unique_orbs = []

            var y = 0
            while (y != y_max) {

              var x = 0
              while (x != x_max) {

                var pixel = image.getPixel( 96 + (177 * x) , 87 + (177 * y) )
                var rgb = `[${roundNearestPowerOfTen(pixel.r)}, ${roundNearestPowerOfTen(pixel.g)}, ${roundNearestPowerOfTen(pixel.b)}]`
                orbs.push(rgb)
                if (unique_orbs.indexOf(rgb) == -1) { unique_orbs.push(rgb) }

                x += 1
              }

              y += 1
            }

            // console.log(orbs)
            // console.log(unique_orbs)

            console.log('orbs length:', orbs.length)
            console.log('unique_orbs length:', unique_orbs.length)
            if (unique_orbs.length > 6) { console.log('\n!!!\nWARNING: IRREGULAR UNIQUE ORB COUNT\n!!!\n') }

            var optimizer_board = []
            var optimizer_orbs = orbs.map((rgb) => ORB_OPTIMIZER_DICTIONARY[rgb])

            while (optimizer_orbs.length > 0) { optimizer_board.push(optimizer_orbs.splice(0, BOARD_SIZE[0])) }

            console.log(optimizer_board)

            // var optimizer_board = new Array(BOARD_SIZE[0])
            // for (var i = 0; i < BOARD_SIZE[0]; i++) { optimizer_board[i] = [] }
            // console.log(optimizer_board)

            // orbs.map((rgb) => ORB_OPTIMIZER_DICTIONARY[rgb]).forEach()

            // fs.writeFile('data.js', 'window.global_android_board = [' + optimizer_board + ']', (err) => {
            fs.writeFile('data.js', 'window.global_android_board = ' + JSON.stringify(optimizer_board) , (err) => {
              if (err) throw err
              console.log('data.json saved')
            })

            fs.appendFile('orbs.txt', '\n\n[' + orbs.join (', ') + ']', (err) => {
              if (err) throw err
              console.log('orbs.txt saved')
            })

            fs.appendFile('unique_orbs.txt', '\n\n[' + unique_orbs.join (', ') + ']', (err) => {
              if (err) throw err
              console.log('unique_orbs.txt saved')
            })

            // image.batch()
            //   // .crop(0, 0, image.width(), image.height())
            //   // .crop(0, 0, 96 + (177 * 5) , image.height())
            //   .crop(0, 0, image.width(), 87 + (177 * 5) )
            //   // .crop(board.top_left_orb_x, board.top_left_orb_y, image.width() - board.top_left_orb_x, image.height() - board.top_left_orb_y)
            //   .writeFile(OUTPUT_PATH, function(err) { if (err) { console.log(err) } })

            image.writeFile(OUTPUT_PATH, function(err) { if (err) { console.log(err) } })

          })
          // .writeFile('output.png', function(err) { if (err) { console.log(err) } })

      })

    })

  })
  .catch(function(err) { console.error('Something went wrong:', err.stack) })
