var fs = require('fs')
var adb = require('adbkit')

const SCREENCAP_PATH = 'manual_screencap.png'

var client = adb.createClient()
var Promise = require('bluebird')

client.listDevices()

  .then(function(devices) {
  	var device_id = devices[0].id
  	return client.screencap(device_id)
  })

  .then(function(data_stream) {
  	data_stream.pipe(fs.createWriteStream(SCREENCAP_PATH))
  })

  .catch(function(err) { console.error('Something went wrong:', err.stack) })
